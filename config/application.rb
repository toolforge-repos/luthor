require_relative "boot"

require "rails/all"

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Luthor
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 6.1

    # Configuration for the application, engines, and railties goes here.
    #
    # These settings can be overridden in specific environments using the files
    # in config/environments, which are processed later.
    #
    # config.time_zone = "Central Time (US & Canada)"
    # config.eager_load_paths << Rails.root.join("extras")
    config.constants = config_for(:constants)
    config.langs = config_for(:languages)
    config.sorted_langs = config.langs.sort_by{|k,v| v[:language_name_en]}
    config.iso_by_qid = config.langs.invert.map{|k,v| [k[:language_qid], v]}.to_h
  end
end
