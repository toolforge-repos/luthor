Rails.application.routes.draw do

  get 'usage/welcome'
  get 'usage/lexeme'
  get 'usage/review'
  get 'usage/lookup_lexeme'
  post 'usage/submit_usage'
  post 'set_ignore_case' => 'application#set_ignore_case'

  # Reveal health status on /up that returns 200 if the app boots with no exceptions, otherwise 500.
  # Can be used by load balancers and uptime monitors to verify that the app is live.
  get "up" => "rails/health#show", as: :rails_health_check

  # sessions and OAuth2
  match '/oauth2/callback', to: 'sessions#create' , via: [:get, :post]

  # Defines the root path route ("/")
  root "usage#welcome"
end
