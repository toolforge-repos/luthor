# To-dos and future plans

## Definitely planned features
* allow adding more than one usage example per lexeme (for other senses, or even another one for the same sense)
* log all added claims (user, lexeme, example[s] added)
** allow any *other* user to review user's added claims [if review pool exhausted, offer already-reviewed edits for yet another review?]
  a *review* feature, where the tool would allow users to *review* other users' added examples and +1 or -1 them. The most -1ed examples can be fed into a report (on-wiki, or in a weekly email to interested people) for people to consider removing them.
** the +1s can add up to a "score" for the reviewed user, and eventually high-scoring users can be given less scrutiny or feature on a scoreboard (as can prolific reviewers).
* allow fetching a longer snippet on-demand (e.g. for incomplete sentences)
* allow switching back and forth between default mode and exact-match mode in Wikisource searches
* allow post-search filtering of snippets not containing the precise lexeme between word boundaries (i.e. also not as substring)
* allow searching for a particular lexeme to work on, even if it already has usage examples.

## Ideas
* filter out obviously unsuitable examples (e.g. redirect or disambiguation pages)
* in the reference try to also add stated-in *if* the Wikisource text has a Wikidata item
* prevent concurrency in a less lazy way (e.g. cache a bunch and round-robin assignments)
