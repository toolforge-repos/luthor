require 'oauth2'
require 'securerandom'

class UsageController < ApplicationController
  def welcome
    client = OAuth2::Client.new(Rails.configuration.constants.oauth_consumer_token, Rails.configuration.constants.oauth_consumer_secret, site: "https://meta.wikimedia.org/w/rest.php", authorize_url: 'oauth2/authorize', token_url: 'oauth2/access_token' , logger: Logger.new('oauth2.log', 'weekly'))
    @oauth_url = client.auth_code.authorize_url(redirect_uri: Rails.configuration.constants.oauth_callback_url)
  end

  def lexeme
    if session[:access_token].present? || ENV['DEBUG'].present?
      @lang = params[:lang]
      if @lang.present?
        @ignore_case = session[:ignore_case].nil? ? true : session[:ignore_case]
        i = 0
        until @usage_examples.present? or i > 10 do # give up after 10 tries
          lex = find_lexemes_missing_usage_examples(@lang)[rand(100)] # TODO: prevent concurrency in a less lazy way (e.g. cache a bunch and round-robin assignments)
          unless lex.nil?
            @lexeme_id = lex[:lexemeId].to_s.split('/').last
            @lemma = lex[:lemma].to_s
            @lexeme = get_lexeme(@lexeme_id)
            @usage_examples = reject_unwanted_examples(get_usage_examples(@lexeme, @lang))
            next if @usage_examples.empty? # try again if we didn't find any good examples
            @senses = @lexeme['senses']
            @lexical_category = get_label_for(@lexeme['lexicalCategory'])
          else
            flash[:notice] = t('usage.no_more_lexemes')
            redirect_to '/'
            return
          end
          i += 1
        end
      elsif params[:lid].present?
        @lexeme_id = params[:lid].strip
        @lexeme = get_lexeme(@lexeme_id)
        @lang = iso_code_by_lang_qid(@lexeme['language'])
        @lemma = @lexeme['lemmas'].first.last['value'] # TODO: support more complicated cases?
        @usage_examples = reject_unwanted_examples(get_usage_examples(@lexeme, @lang))
        @senses = @lexeme['senses']
        @lexical_category = get_label_for(@lexeme['lexicalCategory'])
      end
    else
      flash[:error] = t('usage.must_login_first')
      redirect_to '/'
    end
  end

  # look up a lexeme by lemma string OR by Lexeme ID
  def lookup_lexeme
    if session[:access_token].present? || ENV['DEBUG'].present?
      if params[:q].present?
      lex = RestClient.get 'https://wikidata.org/w/api.php', {params: {action: 'wbsearchentities', search: params[:q], language: I18n.locale.to_s, format: 'json', type: 'lexeme'}}
      if JSON.parse(lex.body)['search'].present?
        lexeme = JSON.parse(lex.body)['search'][0]
        redirect_to '/usage/lexeme?lid='+lexeme['id']
      else
        flash[:error] = t('usage.no_lexeme_found')
        redirect_to '/'
      end
    elsif params[:lid].present?
      redirect_to '/usage/lexeme?lid='+params[:lid]
    else
      flash[:error] = t('usage.no_lexeme_found')
      redirect_to '/'
    end
  else
    flash[:error] = t('usage.must_login_first')
    redirect_to '/'
  end
end

  def review
    
  end
  def submit_usage
    # TODO: handle new sense first
    #Rails.logger.info "Access token: #{session[:access_token]}"
    if session[:access_token].present? || ENV['DEBUG'].present?
      client = MediawikiApi::Client.new Rails.configuration.constants.wikibase
      client.oauth_access_token(session[:access_token]) # depends on mediawiki_api gem v0.8.0 or later
      statement_id = params[:lexeme_id]+'$'+SecureRandom.uuid
      #statement_id = 'L123$'+SecureRandom.uuid # TODO: use real lexeme ID; L123 is the sandbox item, for development
      sense_id = params[:sense_id]
      notice = ''
      if params[:new_sense_gloss].present?
        new_sense_id = create_new_sense(client, params[:lexeme_id], params[:new_sense_lang], params[:new_sense_gloss])
        if new_sense_id.present?
          sense_id = new_sense_id
          notice = t('usage.new_sense_added_successfully')+"\n"
        else
          flash[:error] = t('usage.error_adding_sense', msg: res.body)
          redirect_to '/usage/lexeme?lang='+params[:lang]
        end
      end
      unless flash[:error].present? # skip adding usage example if we couldn't add the new sense
        claim = { id: statement_id, type: "statement", 
          "mainsnak": { 
            "snaktype": "value", "property": "P5831", "datavalue": { 
              "value": { "text": params[:example].strip, "language": params[:lang] }, 
              "type": "monolingualtext" }, 
              "datatype": "monolingualtext" }, 
            "qualifiers": { 
              "P6072": [ { "snaktype": "value", "property": "P6072", "datavalue": { 
                "value": { "entity-type": "sense", "id": sense_id }, "type": "wikibase-entityid" }, "datatype": "wikibase-sense" } ] 
              }, 
            "qualifiers-order": [ "P6072" ], "rank": "normal",
            "references": [
              {
                "snaks": {
                  "P854": [
                    {
                      "snaktype": "value",
                      "property": "P854",
                      "datavalue": {
                        "value": iri_escape(params[:example_url]),
                        "type": "string"
                      },
                      "datatype": "url"
                    }
                  ],
                  "P813": [
                    {
                      "snaktype": "value",
                      "property": "P813",
                      "datavalue": {
                        "value": {
                          "time": Time.now.strftime("+%Y-%m-%dT00:00:00Z"),
                          "timezone": 0,
                          "before": 0,
                          "after": 0,
                          "precision": 11,
                          "calendarmodel": "http://www.wikidata.org/entity/Q1985727"
                        },
                        "type": "time"
                      },
                      "datatype": "time"
                    }
                  ]
                },
                "snaks-order": [
                  "P854",
                  "P813"
                ]
              }
            ]
          }.to_json
        #client.log_in Rails.configuration.constants.dev_username, Rails.configuration.constants.dev_password
        begin
          res = client.action(:wbsetclaim, token_type: "csrf", 'claim' => claim)
          if res.status == 200
            flash[:notice] = flash[:notice] ? flash[:notice]+"\n" : ''
            flash[:notice] += "#{t('usage.example_added_successfully')} - <a href=\"https://wikidata.org/wiki/L:#{params[:lexeme_id]}\">#{params[:lexeme_id]}</a>"
          else
            flash[:error] = t('usage.error_adding_example', msg: res.body)
          end
        rescue MediawikiApi::ApiError => e
          flash[:error] = t('usage.error_adding_example', msg: $!)
          invalidate_session # probably the OAuth token expired
          redirect_to '/'
        end
      end
      redirect_to '/usage/lexeme?lang='+params[:lang] unless session[:access_token].nil?
    else
      flash[:error] = t('usage.must_login_first')
      redirect_to '/'
    end
  end
  protected
  def create_new_sense(client, lexeme_id, lang, gloss)
    #client.log_in Rails.configuration.constants.dev_username, Rails.configuration.constants.dev_password
    data = {glosses: {}}
    data[:glosses][lang] = { "value": gloss, "language": lang}
    res = client.action(:wbladdsense, token_type: "csrf", lexemeId: lexeme_id, data: data.to_json)
    if res.status == 200
      flash[:notice] = t('usage.new_sense_added_successfully')
      return res.data['sense']['id']
    else
      flash[:error] = t('usage.error_adding_sense', msg: res.body)
      return nil
    end
  end
  def get_label_for(qid)
    z = RestClient.get 'https://wikidata.org/w/api.php', {params: {action: 'wbgetentities', ids: qid, format: 'json'}}
    labels = JSON.parse(z.body)['entities'][qid]['labels']
    lang = I18n.locale.to_s
    return labels[lang]['value'] if labels[lang].present? # try to return label in current locale
    return labels['en']['value'] if labels['en'].present?
    return labels.first[1]['value']
  end
  def iri_escape(s)
    s.gsub(' ','_').gsub('"','%22').gsub('[', '%5B').gsub(']','%5D')
  end

  def reject_unwanted_examples(examples)
    # remove examples that are too short or too long
    unless @ignore_case
      examples.select! { |e| e['snippet'].include?(@lemma) }
    end
    return examples
  end
end
